FROM node:16-slim

RUN apt-get update && apt-get install -y python3 make g++ && apt-get clean

WORKDIR /app

COPY package.json yarn.lock ./
RUN --mount=type=cache,target=/root/.yarn YARN_CACHE_FOLDER=/root/.yarn yarn --production --pure-lockfile

COPY . ./
RUN yarn build --no-lint

EXPOSE 3000
CMD ["yarn", "start"]
